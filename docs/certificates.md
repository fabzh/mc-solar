# Certificates

MC Solar project comes with a set of predefined certificates.

This section describes how to renew these certificates or to create new ones if needed.

## Certificates creation

### CA certificate

```shell
$ openssl genrsa -out ca.key 2048
$ cat > ca.conf <<EOF
[ req ]
prompt = no
utf8 = yes
default_md = sha256
days = 3650
distinguished_name = my_distinguished_name
req_extensions = my_extensions

[ my_distinguished_name ]
CN = CA

[ my_extensions ]
subjectKeyIdentifier = hash
basicConstraints = CA:true
EOF
$ openssl req -new -x509 -key ca.key -out ca.crt -config ca.conf
```

### Mosquitto certificate

```shell
$ openssl genrsa -out mosquitto.key 2048
$ cat > mosquitto.conf <<EOF
[ req ]
prompt = no
utf8 = yes
default_md = sha256
days = 3650
distinguished_name = my_distinguished_name
req_extensions = my_extensions

[ my_distinguished_name ]
CN = mosquitto

[ my_extensions ]
basicConstraints = CA:false
EOF
$ openssl req -new -key mosquitto.key -out mosquitto.csr -config mosquitto.conf
$ openssl x509 -req -in mosquitto.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out mosquitto.crt
```

### OpenDTU certificate

```shell
$ openssl genrsa -out opendtu.key 2048
$ cat > opendtu.conf <<EOF
[ req ]
prompt = no
utf8 = yes
default_md = sha256
days = 3650
distinguished_name = my_distinguished_name
req_extensions = my_extensions

[ my_distinguished_name ]
CN = opendtu

[ my_extensions ]
basicConstraints = CA:false
EOF
$ openssl req -new -key opendtu.key -out opendtu.csr -config opendtu.conf
$ openssl x509 -req -in opendtu.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out opendtu.crt
```

### MC Solar certificate

```shell
$ openssl genrsa -out mcsolar.key 2048
$ cat > mcsolar.conf <<EOF
[ req ]
prompt = no
utf8 = yes
default_md = sha256
days = 3650
distinguished_name = my_distinguished_name
req_extensions = my_extensions

[ my_distinguished_name ]
CN = mcsolar

[ my_extensions ]
basicConstraints = CA:false
EOF
$ openssl req -new -key mcsolar.key -out mcsolar.csr -config mcsolar.conf
$ openssl x509 -req -in mcsolar.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out mcsolar.crt
```

## Certificates renewal

### CA certificate

```shell
$ cat > ca.conf <<EOF
[ req ]
prompt = no
utf8 = yes
default_md = sha256
days = 3650
distinguished_name = my_distinguished_name
req_extensions = my_extensions

[ my_distinguished_name ]
CN = CA

[ my_extensions ]
subjectKeyIdentifier = hash
basicConstraints = CA:true
EOF
$ openssl req -new -x509 -key ca.key -out ca.crt -config ca.conf
```

### Mosquitto certificate

```shell
$ openssl x509 -req -in mosquitto.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out mosquitto.crt
```

### OpenDTU certificate

```shell
$ openssl x509 -req -in opendtu.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out opendtu.crt
```

### MC Solar certificate

```shell
$ openssl x509 -req -in mcsolar.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out mcsolar.crt
```