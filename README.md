# MC Solar

## What is MC Solar ?

MC Solar stands for Management Console Solar.

Its features are :
  - Provides a MQTT server that allows OpenDTU to publish data
  - Provides a MQTT client that subscribes to OpenDTU data
  - Provides an ElasticSearch database to store data
  - Provides a kibana portal to create dashboards

Here is the global synopsis:

![Global synopsis](docs/illustrations/mcsolar_synopsis.drawio.svg)

## Deployment

### Prerequisites

Clone the project.

```shell
$ git clone git@gitlab.com:fabzh/mc-solar.git
$ cd mc-solar
```

Create secrets files.

```shell
$ mkdir secrets
$ echo 'some_password' > secrets/mcsolar_password
$ echo 'some_password' > secrets/opendtu_password
```

### Mosquitto setup

Create the service.

```shell
$ docker compose create mosquitto
```

Create the password file.

```shell
$ docker compose run mosquitto mosquitto_passwd -c /mosquitto/config/password_file mcsolar
$ docker compose run mosquitto mosquitto_passwd /mosquitto/config/password_file opendtu
```

Copy the configuration file (you'll be asked to enter the password twice).

```shell
$ docker compose cp mosquitto/files/mosquitto.conf mosquitto:/mosquitto/config/mosquitto.conf
$ docker compose cp mosquitto/files/acl_file mosquitto:/mosquitto/config/acl_file
$ docker compose cp tls/ca.crt mosquitto:/mosquitto/config/ca.crt
$ docker compose cp tls/mosquitto.crt mosquitto:/mosquitto/config/mosquitto.crt
$ docker compose cp tls/mosquitto.key mosquitto:/mosquitto/config/mosquitto.key
```

Start mosquitto.

```shell
$ docker compose start mosquitto
```

### Connector setup

Just create and start the service.

```shell
$ docker compose up connector -d
```

### Elasticsearch setup

Create the service.

```shell
$ docker compose create elasticsearch
```

Change the owner of data directory.

```shell
$ docker compose run --user root elasticsearch chown -R elasticsearch /usr/share/elasticsearch/data
```

Start the service.

```shell
$ docker compose start elasticsearch
```

### Grafana setup

Just create and start the service.

```shell
$ docker compose up grafana -d
```