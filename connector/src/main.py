from document import get_document
import elasticsearch
import hashlib
import os
import paho.mqtt.client as mqtt
import time

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    doc = get_document(msg.topic, msg.payload.decode('utf-8'))
    doc_id = get_id_digest(str(doc))
    resp = es.index(index='mcsolar', id=doc_id, body=doc)
    print(resp['result'])

def get_id_digest(id_txt):
    sha2 = hashlib.sha256()
    sha2.update(id_txt.encode('utf-8'))
    return sha2.hexdigest()

password_file = os.environ.get('MCSOLAR_PASSWORD_FILE')
with open(password_file, 'r', encoding='utf8') as f:
    password = f.read()

retry_connection_delay = 3
es = elasticsearch.Elasticsearch(['http://elasticsearch:9200'])

# Loop : try to create indices until elasticsearch is ready
while True:
    try:
        print(f"Trying to connect...")
        es.indices.create(index='mcsolar', ignore=400)
        break
    except elasticsearch.exceptions.ConnectionError as e:
        print(e)
        print(f"Waiting {retry_connection_delay}s before trying again...")
        time.sleep(retry_connection_delay)
print("Connected")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set("mcsolar", password)


# client.tls_set(
#     ca_certs='ca.crt',
#     certfile='mcsolar.crt',
#     keyfile='mcsolar.key'
# )

client.connect("mosquitto", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()