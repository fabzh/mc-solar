from datetime import datetime
import re

"""
According to MC Solar principle, a document is created for each message received
from mosquitto.

A document is represented by a dict. Keys contained in that dict are not the
same for all documents. Documents which have the same set of keys are associated
to the same document type.

Each document type is associated to a name.

Each document type is associated to a regex. This regex is intended to match the
topic in order to associate a message to a document type.

The set of keys associated to a document type is defined according to the rules
below:
    1. There is always a key named 'datetime'. Value associated to this key is
       the datetime when the message is received.
    2. There is always a key named 'base'. Value associated to this key is the
       first level of the topic.
    3. There is always a key named 'type'. Value associated to this key is the
       document type name.
    4. The last level of the topic is always considered as a parameter name.
       This parameter name defines a key name in the document. Value associated
       to this key is given by the payload of the message. Depending on the
       parameter, the type of the value may be int, str, float or datetime.
    5. There may be keys whose values are parsed from topic.
"""

class ErroneousDocument(Exception):
    pass

class Doctype:
    """
    This class describes a document type.

    Attributes
    ----------

    name: str
        Name of the document type.

    optional_params: dict
        Keys (int) are the number of group parsed by the re attribute.
        Values are list of 2 str:
            - 1st str : name of the optional parameter
            - 2nd str : type of the optional parameter
    
    params: dict
        Keys (str) are the possible parameter names for the document type.
        Values (str) depicte the type of parameter value.

    re: re.Pattern
        The pattern is compile from the raw string passed to the constructor.
        This pattern is intended to match the topic.        
    """

    def __init__(self, name, raw_str, params, opt_params={}):

        self.name = name
        self.re = re.compile(raw_str)
        self.params = params
        self.opt_params = opt_params

# IMPORTANT
# Document types have to be sorted : most specific regex first.

DOCTYPES = [
    Doctype(
        'global_ac',
        r'^([^/]+)/ac/([^/]+)$',
        {
            'is_valid': 'int',
            'power': 'float',
            'yieldday': 'float',
            'yieldtotal': 'float',
        }
    ),
    Doctype(
        'global_dc',
        r'^([^/]+)/dc/([^/]+)$',
        {
            'is_valid': 'int',
            'irradiation': 'float',
            'power': 'float',
        }
    ),
    Doctype(
        'dtu',
        r'^([^/]+)/dtu/([^/]+)$',
        {
            'bssid': 'str',
            'hostname': 'str',
            'ip': 'str',
            'rssi': 'int',
            'status': 'str',
            'uptime': 'int',
        }
    ),
    Doctype(
        'inverter',
        r'^([^/]+)/([^/]+)/(0/)?([^/]+)$',
        {
            'name': 'str',
            'current': 'float',
            'efficiency': 'float',
            'frequency': 'float',
            'power': 'float',
            'powerdc': 'float',
            'powerfactor': 'float',
            'reactivepower': 'float',
            'temperature': 'float',
            'voltage': 'float',
            'yieldday': 'float',
            'yieldtotal': 'float',
        },
        {
            2: ['serial', 'str']
        }
    ),
    Doctype(
        'inverter_device',
        r'^([^/]+)/([^/]+)/device/([^/]+)$',
        {
            'bootloaderversion': 'int',
            'fwbuilddatetime': 'datetime',
            'fwbuildversion': 'int',
            'hwpartnumber': 'str',
            'hwversion': 'str',
        },
        {
            2: ['serial', 'str']
        }
    ),
    Doctype(
        'inverter_status',
        r'^([^/]+)/([^/]+)/status/([^/]+)$',
        {
            'last_update': 'int',
            'limit_absolute': 'float',
            'limit_relative': 'float',
            'producing': 'int',
            'reachable': 'int',
        },
        {
            2: ['serial', 'str']
        }
    ),
    Doctype(
        'inverter_line',
        r'^([^/]+)/([^/]+)/([1-4])/([^/]+)$',
        {
            'current': 'float',
            'irradiation': 'float',
            'name': 'str',
            'power': 'float',
            'voltage': 'float',
            'yieldday': 'float',
            'yieldtotal': 'float',
        },
        {
            2: ['serial', 'str'],
            3: ['number', 'int']
        }
    ),
]

def get_document(topic, payload) -> dict:
    """
    The aims of this function are :
        - parsing of topic and payload
        - identifying the type of document to create
        - creating the document
        - returning the document related to the received message

    Parameters
    ----------

    topic: str
        Topic of a received message.

    payload: str
        Payload of the received message.

    Return
    ------

    dict : Document related to the received message
    """

    # Document initialization
    doc = {}

    # Walk through the document types
    for doctype in DOCTYPES:

        # Apply the pattern
        m_res = doctype.re.match(topic)

        # Is pattern doesn't match, let's continue
        if not m_res:
            continue

        # datetime value is now !
        doc['datetime'] = datetime.now()

        # base value is always the 1st level of topic
        doc['base'] = m_res.group(1)

        # type value is given by doctype
        doc['type'] = doctype.name

        # Parameter name is always the last group matched in the regex
        param_name = m_res.groups()[-1]

        # If parameter name is not a possible parameter name, let's continue
        if not param_name in doctype.params:
            continue

        # Get the expected type for the param value
        value_type = doctype.params[param_name]

        # Get param value demending on value type
        if value_type == 'int':
            param_value = int(payload)

        elif value_type == 'str':
            param_value = payload

        elif value_type == 'float':
            param_value = float(payload)

        elif value_type == 'datetime':
            # Expected payload format : 2022-08-26 20:51:00
            re_date = re.compile(r'(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)')
            m_date = re_date.match(payload)
            if not m_date:
                continue
            param_value = datetime(
                int(m_date.group(1)),
                int(m_date.group(2)),
                int(m_date.group(3)),
                int(m_date.group(4)),
                int(m_date.group(5)),
                int(m_date.group(6))
            )
        
        doc[param_name] = param_value

        # Walk through optional params
        for topic_level in doctype.opt_params:

            opt_param_name = doctype.opt_params[topic_level][0]
            opt_value_type = doctype.opt_params[topic_level][1]

            opt_payload = m_res.group(topic_level)

            # Get opt param value demending on opt value type
            if opt_value_type == 'int':
                opt_value = int(opt_payload)

            elif opt_value_type == 'str':
                opt_value = opt_payload

            elif opt_value_type == 'float':
                opt_value = float(opt_payload)

            elif opt_value_type == 'datetime':
                # Expected opt_payload format : 2022-08-26 20:51:00
                re_date = re.compile(r'(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)')
                m_date = re_date.match(opt_payload)
                if not m_date:
                    continue
                opt_value = datetime(
                    int(m_date.group(1)),
                    int(m_date.group(2)),
                    int(m_date.group(3)),
                    int(m_date.group(4)),
                    int(m_date.group(5)),
                    int(m_date.group(6))
                )
            doc[opt_param_name] = opt_value

        return doc

    raise(ErroneousDocument)
